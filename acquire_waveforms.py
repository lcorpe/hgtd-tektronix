#!/usr/bin/env python3
# Authors: Mateus Vicente and Thorben Quast
# Created: XX Feb 2021
# Last modified: 23 Feb 2021
# Description: 
# 1. Configures fast frame of oscilloscope
# 2. Acquires N waveforms and saves them to the oscilloscope's local FS
# 3. Transfers raw data (as csv) and screenshot to the client PC.
import time, sys, os, argparse, time
import pyvisa as visa 

start_time = time.time()
def log(msg=None, ref_time=start_time):
    time_msg = "[%.1fs]" % (time.time() - start_time)
    if msg == None:
        print(time_msg)
    else:
        print(time_msg+" "+str(msg))


parser = argparse.ArgumentParser()
parser.add_argument("--VISASourceID", type=str,
                    help="(input) ID of the VISA resource from which the data is pulled", default="TCPIP::128.141.204.47::INSTR", required=False)
parser.add_argument("--NWaveForms", type=int,
                    help="(option) Number of waveforms to acquire.", default=500, required=False)
parser.add_argument("--DataCSV", type=str,
                    help="(output) Raw data file as csv", default="waveform", required=False)
parser.add_argument("--OutputDir", type=str,
                    help="Where to store the data", default="./waveforms", required=False)
parser.add_argument("--Index", type=int,
                    help="Index for this file", default=0, required=False)
args = parser.parse_args()

maxSegments = 250
nCollected = 0

os.system("mkdir -p %s" % args.OutputDir)

#open the VISA source
try:
    rm = visa.ResourceManager()
    scope = rm.open_resource(args.VISASourceID)
    log(scope.query('*IDN?')) 
except:
    import sys
    sys.exit("No VISA resource available under %s.\n Program aborted." % args.VISASourceID)


#configure the acquisition
frameSize = scope.query('horizontal:acqlength?') #get frame length
numFrames = maxSegments
scope.write('horizontal:fastframe:count {}'.format(numFrames)) #set number of frames
scope.write('horizontal:fastframe:state 1') #turn on fast frame

while nCollected < args.NWaveForms: 
  
  args.Index += 1
  #acquire a set of frames and then stop acquiring
  scope.write('acquire:state 0')
  scope.write('acquire:stopafter SEQUENCE')
  scope.write('acquire:state 1')
  bob = True
  while bob:
      try:
          scope.query('*opc?')
          bob = False
      except:
          log("...acquiring waveforms")
          bob = True
          time.sleep(0.1)     #sleeping for 100ms
      
  log("Acquisition complete. Saving data to spreadsheet.")
      
  #save the data
  scope.write('save:waveform:fileformat SPREADSHEETCsv') #set format before data start/stop
  log("Data integrity check")
  scope.write('save:waveform:data:start 1') #ensure data range is good
  scope.write('save:waveform:data:stop {}'.format(frameSize)) #ensure data range is good
  
  log("Saving waveform on oscilloscope")
  filename = r'C:\Frame_all.csv'
  scope.write('save:waveform ALL,"{}"'.format(filename))
  scope.query('*opc?')
  
  #transfer file to local PC
  log("Transfering file to local filesystem")
  scope.write('FILESystem:READFile \"C:\Frame_all_ALL.csv\"')
  waveform = scope.read_raw()
  
  file = open("%s/%s_%d.csv" % (args.OutputDir, args.DataCSV, args.Index), "wb")
  file.write(waveform)
  file.close()
  
  log("saved file {}".format("%s/%s_%d.csv" % (args.OutputDir, args.DataCSV, args.Index)))
  
  # Save image to instrument's local disk
  #log("Saving image on oscilloscope")
  #scope.write('SAVE:IMAGe \"C:/Temp.png\"')
  #
  ## Wait for instrument to finish writing image to disk
  #scope.query('*OPC?')
  #
  ## Read image file from instrument
  #log("Transferring image from oscilloscope")
  #scope.write('FILESystem:READFile \"C:/Temp.png\"')
  #imgData = scope.read_raw(1024*1024)
  #
  ## Save image data to local disk
  #file2 = open(args.DataPNG, "wb")
  #file2.write(imgData)
  #file2.close()
  #log("Saved picture {}".format(args.DataPNG))
  #
  #print("DEBUG C")
  # Image data has been transferred to PC and saved. Delete image file from instrument's hard disk.
  log("Delete temporary data from oscilloscope")
  #scope.write('FILESystem:DELEte \"C:/Temp.png\"')
  scope.write('FILESystem:DELEte \"C:/Frame_all.csv\"')
  nCollected +=  maxSegments
  log("So far, %d/%d events collected" % (nCollected, args.NWaveForms))

log("Reset oscilloscope settings.")
scope.write('horizontal:fastframe:state 0') #turn off fast frame
scope.write('acquire:stopafter RUNSTop')
scope.write('acquire:state RUN')

#closing the visa resource managers and the scope
scope.close()
rm.close()
